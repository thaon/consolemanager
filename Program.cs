﻿using System;
using System.Collections.Generic;

namespace ConsoleManager
{
    class Program
    {
        static void Main(string[] args)
        {
            RenderManager manager = new RenderManager(22,22);
            //draw a rectangle around the console
            manager.DrawLine(0, 0, 21, 0, '-');
            manager.DrawLine(0, 21, 21, 21, '-');
            manager.DrawLine(0, 0, 0, 21, '|');
            manager.DrawLine(21, 0, 21, 21, '|');
            manager.DrawLine(0, 0, manager.width-1, manager.height-1, '~');
            manager.DrawAt((int)Math.Floor(manager.width/2.0), (int)Math.Floor(manager.height/2.0)-1, '#');

            manager.Wait();
        }
    }
}
