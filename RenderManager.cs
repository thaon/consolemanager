﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleManager
{
    public class RenderManager
    {
        #region properties

        public int width
        {
            get
            {
                return _width;
            }
        }
        private int _width = 0;

        public int height
        {
            get
            {
                return _height;
            }
        }
        private int _height = 0;

        #endregion

        #region Public methods

        public RenderManager(int width, int height)
        {
            Console.SetWindowSize(width, height);
            Console.BufferHeight = height;
            Console.BufferWidth = width;
            Console.CursorVisible = false;
            _width = width;
            _height = height;
        }

        public void DrawAt(int x, int y, char thing)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(thing);
        }
        /*
        dx = x2 - x1
        dy = y2 - y1
        for x from x1 to x2 
        {
         y = y1 + dy * (x - x1) / dx
         plot(x, y)
         }
         */
        public void DrawLine(int x0, int y0, int x1, int y1, char thing)
        {
            int deltax = x1 - x0;
            int deltay = y1 - y0;
            int x, y;
            if (deltax != 0 && deltay !=0)
                for (x = x0; x < x1; x++)
                {
                    y = y0 + deltay * (x - x0) / deltax;
                    DrawAt(x, y, thing);
                }
            else if (deltay == 0)
            {
                for (x = x0; x < x1; x++)
                    DrawAt(x, y0, thing);
            }
            else if (deltax == 0)
            {
                for (y = y0; y < y1; y++)
                    DrawAt(x0, y, thing);
            }
        }

        public void DrawRectangle(int x, int y, int x1, int y1, char thing)
        {
            Console.SetCursorPosition(x,y);
        }

        public void Wait()
        {
            Console.ReadKey();
        }

        #endregion

        #region Indexer

        #endregion

    }
}
